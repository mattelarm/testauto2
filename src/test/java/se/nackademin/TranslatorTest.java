/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mattias
 */
public class TranslatorTest {
   
    @Test
    public void testGetNoun() {
        Translator translator = new Translator();
        String expResult = "en lönesänkning";
        String result = translator.getNoun(1);
        assertEquals("result should be 'en lönesänkning",expResult, result);
    }
    
    @Test
    public void testGetVerb() {
        Translator translator = new Translator();
        String expResult = "flyga";
        String result = translator.getVerb(2);
        assertEquals("result should be 'flyga'", expResult, result);
    }

    @Test
    public void testGetAdjective() {
        Translator translator = new Translator();
        String expResult = "stor";
        String result = translator.getAdjective(0);
        assertEquals("result should be 'stor'", expResult, result);
    }
    
}
